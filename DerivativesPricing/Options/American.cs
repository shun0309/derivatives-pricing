﻿using DerivativesPricing.PayOff;
using MathNet.Numerics;
using MathNet.Numerics.Distributions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DerivativesPricing.Options
{
    public class American : VanillaOption
    {
        public American()
        {
            Parameters = new Dictionary<string, double>();
            Parameters.Add("Spot price", 0.0D);
            Parameters.Add("Strike price", 0.0D);
            Parameters.Add("Risk-free rate", 0.0D);
            Parameters.Add("Dividend yield", 0.0D);
            Parameters.Add("Volatility", 0.0D);
            Parameters.Add("Time", 0.0D);
            Parameters.Add("Period", 0.0D);
            Parameters.Add("Number of periods", 0.0D);
            Parameters.Add("Number of simulations", 0.0D);
        }
        public override double GetPrice()
        {
            double numOfSimDouble = Parameters["Number of simulations"];
            int numOfSim = Convert.ToInt32(numOfSimDouble);
            double numOfPeriodDouble = Parameters["Number of periods"];
            int numOfPeriod = Convert.ToInt32(numOfPeriodDouble);

            double r = Parameters["Risk-free rate"];
            double q = Parameters["Dividend yield"];
            double v = Parameters["Volatility"];
            double h = Parameters["Period"];
            double s = Parameters["Spot price"];

            double[,] stockArray = new double[numOfSim, numOfPeriod + 1];
            double[,] payoffArray = new double[numOfSim, numOfPeriod + 1];

            for (int i = 0; i < numOfSim; i++)
            {
                for (int j = 0; j <= numOfPeriod; j++)
                {
                    stockArray[i, j] = s * Math.Exp((r - q - 0.5 * v * v) * j * h + v * Math.Sqrt(j * h) * Normal.Sample(0, 1));

                    OptionPayOff optionPayOff = (OptionPayOff)PayOff;
                    optionPayOff.StockPrice = stockArray[i, j];
                    double k = Parameters["Strike price"];
                    optionPayOff.StrikePrice = k;
                    double payOff = PayOff.CalculatePayOff();

                    payoffArray[i, j] = payOff;
                }
            }

            double[,] hv = new double[numOfSim, numOfPeriod + 1];

            for (int y = numOfPeriod - 1; y >= 1; y--)
            {
                List<double> xdata = new List<double>();
                List<double> ydata = new List<double>();
                for (int x = 0; x < 10000; x++)
                {
                    hv[x, y] = payoffArray[x, y + 1] * Math.Exp(-r);
                    if (hv[x, y] != payoffArray[x, y])
                    {
                        xdata.Add(stockArray[x, y]);
                        ydata.Add(hv[x, y]);
                    }
                }
                double[] regressionParams = Fit.Polynomial(xdata.ToArray(), ydata.ToArray(), 2);
                Parallel.For(0, 10000, x =>
                {
                    double expectedHV = regressionParams[0] + regressionParams[1] * stockArray[x, y] + regressionParams[2] * stockArray[x, y] * stockArray[x, y];
                    if (expectedHV > payoffArray[x, y])
                    {
                        payoffArray[x, y] = hv[x, y];
                    }
                });
            }

            double sum = 0;
            for (int z = 0; z < numOfSim; z++)
            {
                sum += payoffArray[z, 1] * Math.Exp(-r) / numOfSim;
            }

            return sum;
        }
    }
}
