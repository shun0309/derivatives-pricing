﻿using DerivativesPricing.PayOff;
using System;
using System.Collections.Generic;

namespace DerivativesPricing.Options
{
    public static class OptionFactory
    {
        private static IDictionary<string, Option> options;

        static OptionFactory()
        {
            options = new Dictionary<string, Option>();
            RegisterOption("European style", new European());
            RegisterOption("American style", new American());
        }

        public static void RegisterOption(string optionName, Option option)
        {
            if (!options.ContainsKey(optionName))
                options.Add(optionName, option);
        }

        public static Option CreateOption(string optionName)
        {
            Option option;
            if (options.ContainsKey(optionName))
                option = options[optionName];
            else
                throw new Exception("Option is not suppported.");
            option.SetParametersByUserInput();

            PayOffFactory.ListAllPayOffs();
            Console.WriteLine("Please input the type of payoff.");
            string payOffType = Console.ReadLine();

            option.PayOff = PayOffFactory.CreatePayOff(payOffType);
            return option;
        }

        public static void ListAllOptions()
        {
            Console.WriteLine("List of option:");
            int i = 1;
            foreach (var pair in options)
            {
                Console.WriteLine("{0}. {1}", i, pair.Key);
                i++;
            }
        }
    }
}
