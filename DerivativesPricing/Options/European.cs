﻿using DerivativesPricing.PayOff;
using MathNet.Numerics.Distributions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace DerivativesPricing.Options
{
    public class European : VanillaOption
    {
        public European()
        {
            Parameters = new Dictionary<string, double>();
            Parameters.Add("Spot price", 0.0D);
            Parameters.Add("Strike price", 0.0D);
            Parameters.Add("Risk-free rate", 0.0D);
            Parameters.Add("Dividend yield", 0.0D);
            Parameters.Add("Volatility", 0.0D);
            Parameters.Add("Time", 0.0D);
            Parameters.Add("Number of simulations", 0.0D);
        }

        public override double GetPrice()
        {
            var buffer = new BufferBlock<double>();
            var consumer = Consume(buffer);
            Produce(buffer);
            consumer.Wait();
            return consumer.Result;
        }

        public void Produce(ITargetBlock<double> target)
        {
            double numOfSimDouble = Parameters["Number of simulations"];
            int numOfSim = Convert.ToInt32(numOfSimDouble);
            try
            {
                for (int i = 0; i < numOfSim; i++)
                {
                    target.Post<double>(Normal.Sample(0, 1));
                }
            }
            finally
            {
                target.Complete();
            }
        }

        public async Task<double> Consume(ISourceBlock<double> Source)
        {
            double averagePayOff = 0;
            double spotPrice = Parameters["Spot price"];
            double riskFreeRate = Parameters["Risk-free rate"];
            double dividendYield = Parameters["Dividend yield"];
            double volatility = Parameters["Volatility"];
            double time = Parameters["Time"];
            double numOfSimDouble = Parameters["Number of simulations"];
            int numOfSim = Convert.ToInt32(numOfSimDouble);

            while (await Source.OutputAvailableAsync())
            {
                double z = Source.Receive();

                double expectedStockPrice = spotPrice * Math.Exp((riskFreeRate - dividendYield - 0.5 * volatility * volatility) * time + volatility * Math.Sqrt(time) * z);

                OptionPayOff optionPayOff = (OptionPayOff)PayOff;
                optionPayOff.StockPrice = expectedStockPrice;
                optionPayOff.StrikePrice = Parameters["Strike price"];
                double payOff = PayOff.CalculatePayOff();

                averagePayOff += payOff / numOfSim;

            }
            return averagePayOff;
        }
    }
}
