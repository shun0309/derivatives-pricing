﻿using DerivativesPricing.PayOff;
using System;
using System.Collections.Generic;

namespace DerivativesPricing.Options
{
    public abstract class Option
    {
        public IPayOff PayOff;
        public IDictionary<string, double> Parameters;

        public void SetParametersByUserInput()
        {
            var keys = new List<string>(Parameters.Keys);
            foreach (string key in keys)
            {
                Console.WriteLine("{0}?", key);
                Parameters[key] = Convert.ToDouble(Console.ReadLine());
            }
        }

        public abstract double GetPrice();
    }
}
