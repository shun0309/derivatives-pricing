﻿using DerivativesPricing.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerivativesPricing
{
    class Program
    {
        static void Main(string[] args)
        {
            OptionFactory.ListAllOptions();
            Console.WriteLine("Please name an option.");
            string optionName = Console.ReadLine();
            Option option = OptionFactory.CreateOption(optionName);
            Console.WriteLine("The price of the option is {0}", option.GetPrice());
        }
    }
}
