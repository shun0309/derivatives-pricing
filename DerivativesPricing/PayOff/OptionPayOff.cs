﻿namespace DerivativesPricing.PayOff
{
    public abstract class OptionPayOff : IPayOff
    {
        public double StrikePrice;
        public double StockPrice;
        public abstract double CalculatePayOff();
    }
}
