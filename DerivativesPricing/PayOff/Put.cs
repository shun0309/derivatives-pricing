﻿using System;

namespace DerivativesPricing.PayOff
{
    class Put : OptionPayOff
    {
        public override double CalculatePayOff()
        {
            return Math.Max(StrikePrice - StockPrice, 0);
        }
    }
}
