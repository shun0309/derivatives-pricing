﻿using System;
using System.Collections.Generic;

namespace DerivativesPricing.PayOff
{
    public static class PayOffFactory
    {
        private static IDictionary<string, IPayOff> payOffs;

        static PayOffFactory()
        {
            payOffs = new Dictionary<string, IPayOff>();
            RegisterPayOff("Call option", new Call());
            RegisterPayOff("Put option", new Put());
        }

        public static void RegisterPayOff(string payOffName, IPayOff payOff)
        {
            if (!payOffs.ContainsKey(payOffName))
                payOffs.Add(payOffName, payOff);
        }

        public static IPayOff CreatePayOff(string payOffName)
        {
            if (payOffs.ContainsKey(payOffName))
                return payOffs[payOffName];
            else
                throw new Exception("The inputed payoff is not suppported.");
        }

        public static void ListAllPayOffs()
        {
            Console.WriteLine("List of payoff:");
            int i = 1;
            foreach (var pair in payOffs)
            {
                Console.WriteLine("{0}. {1}", i, pair.Key);
                i++;
            }
        }
    }
}
