﻿using System;

namespace DerivativesPricing.PayOff
{
    class Call : OptionPayOff
    {
        public override double CalculatePayOff()
        {
            return Math.Max(StockPrice - StrikePrice, 0);
        }
    }
}
