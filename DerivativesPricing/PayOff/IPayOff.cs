﻿namespace DerivativesPricing.PayOff
{
    public interface IPayOff
    {
        double CalculatePayOff();
    }
}
