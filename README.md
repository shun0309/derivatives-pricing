# README #

### What is this repository for? ###

* Quick summary: A C# console application which supports derivatives pricing
* Version: 1.0

### Features ###
* Monte Carlo European Call/Put: a robust producer-consumer pattern using the TPL Dataflow Library to generate large number of standard normal random variable for payoff calculation
* Monte Carlo American Call/Put: using least square method to estimate the pay-off accelerated by parallel programming

### How do I get set up? ###

* Step 1: Download the source code and open in Visual Studio. 
* Step 2: Open nuget Package Manager Console and reinstall Math.NET Numerics. i.e. Run "Uninstall-Package MathNet.Numerics" and "Install-Package MathNet.Numerics"
* Step 3: Rebuild the solution.
* Step 4: Run the program.

### Remarks ###

* Dependencies: .NET 4.5 and Visual Studio 2013, or higher
* Libraries: Math.NET Numerics, Microsoft TPL Dataflow